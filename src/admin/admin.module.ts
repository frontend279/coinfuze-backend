import { Module } from '@nestjs/common';
import { AdminService } from './admin.service';
import { AdminController } from './admin.controller';
import { PrismaService } from "../prisma.service";
import { AdminStrategy } from "../auth/strategies/admin.strategy";
import { PlansService } from "../plans/plans.service";

@Module({
  controllers: [AdminController],
  providers: [AdminService, PrismaService, PlansService],
})
export class AdminModule {}
