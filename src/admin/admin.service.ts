import { Injectable } from '@nestjs/common';
import { PrismaService } from '../prisma.service';

@Injectable()
export class AdminService {
  constructor(private prisma: PrismaService) {}

  async getAllUsers() {
    const db = await this.prisma.users.findMany({
      select: {
        id: true,
        createdAt: true,
        login: true,
        role: true,
        email: true,
        userInfo: {
          select: {
            avatar: true,
            phone: true,
          },
        },
      },
    });
    return db;
  }

  async getCurrentUser(id: number) {
    const db = await this.prisma.users.findUnique({
      where: {
        id: +id,
      },
      select: {
        login: true,
        balance: true,
        userInfo: true,
        transactions: true,
        Bans: true,
      },
    });
    return db;
  }
}
