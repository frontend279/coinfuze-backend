import {
  Body,
  Controller,
  Get,
  HttpCode,
  HttpStatus,
  Param, Patch,
  Post,
  UseGuards
} from "@nestjs/common";
import { AdminService } from './admin.service';
import { AdminAuthGuard } from '../auth/guards/admin.guard';
import { PlansService } from '../plans/plans.service';
import { CreatePlanDto } from '../plans/dto/create.dto';
import { UpdatePlanDto } from "../plans/dto/update.dto";

@Controller('admin')
export class AdminController {
  constructor(
    private readonly adminService: AdminService,
    private readonly plansService: PlansService,
  ) {}

  @Get('users')
  @UseGuards(AdminAuthGuard)
  @HttpCode(HttpStatus.OK)
  async getUsers() {
    return await this.adminService.getAllUsers();
  }

  @Get('user/:id')
  @UseGuards(AdminAuthGuard)
  @HttpCode(HttpStatus.OK)
  async getUser(@Param('id') id: number) {
    return await this.adminService.getCurrentUser(id);
  }

  @Post('plans/create')
  @UseGuards(AdminAuthGuard)
  @HttpCode(HttpStatus.OK)
  async createPlan(@Body() createPlanDto: CreatePlanDto) {
    return await this.plansService.create(createPlanDto);
  }

  @Get('plans')
  @UseGuards(AdminAuthGuard)
  @HttpCode(HttpStatus.OK)
  async plansList() {
    return await this.plansService.findAll();
  }

  @Get('plans/:id')
  @UseGuards(AdminAuthGuard)
  @HttpCode(HttpStatus.OK)
  async planId(@Param('id') id: string) {
    return await this.plansService.findOne(+id);
  }

  @Patch('plans/:id')
  @UseGuards(AdminAuthGuard)
  @HttpCode(HttpStatus.OK)
  async changePlan(@Param('id') id: string, @Body() data: UpdatePlanDto) {
    return await this.plansService.changeOne(+id, data);
  }
}
