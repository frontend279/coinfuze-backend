import { IncomingHttpHeaders } from 'http';
import * as useragent from 'useragent';

export const getHeadersInfo = (headers: IncomingHttpHeaders) => {
  const userAgentInfo = useragent.parse(headers['user-agent']);

  return {
    os: userAgentInfo.os.toString(),
    userAgent: headers['user-agent'],
    browser: userAgentInfo.toAgent().toString(),
  };
};
