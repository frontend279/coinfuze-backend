export const formatTimeString = (time: Date) => {
  return {
    date:
      ('0' + time.getDate()).slice(-2) +
      '.' +
      ('0' + (time.getMonth() + 1)).slice(-2) +
      '.' +
      time.getFullYear(),
    time: time.getHours() + ':' + time.getMinutes() + ':' + time.getSeconds(),
  };
};
