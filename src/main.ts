import { NestFactory, Reflector } from "@nestjs/core";
import { AppModule } from './app.module';
import { ValidationPipe } from '@nestjs/common';
import { AtAuthGuard } from './auth/guards/at.guard';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors();
  const reflector = new Reflector();
  app.useGlobalGuards(new AtAuthGuard(reflector));
  await app.listen(2000);
}
bootstrap();
