import { Module } from '@nestjs/common';
import { AuthService } from './auth.service';
import { AuthController } from './auth.controller';
import { PrismaService } from '../prisma.service';
import { JwtModule } from '@nestjs/jwt';
import { AtStrategy } from './strategies/at.strategy';
import { RTStrategy } from "./strategies/rt.strategy";
import { UsersService } from "../users/users.service";
import { PassportModule } from "@nestjs/passport";
import { AdminStrategy } from "./strategies/admin.strategy";

@Module({
  imports: [
    JwtModule.register({}),
  ],
  controllers: [AuthController],
  providers: [AuthService, PrismaService, AtStrategy, RTStrategy, UsersService, AdminStrategy],
})
export class AuthModule {}
