import {
  Body,
  Controller,
  Delete,
  Get,
  HttpCode,
  HttpStatus,
  Ip,
  Post,
  Req,
  UseGuards,
} from '@nestjs/common';
import { AuthService } from './auth.service';
import { createUserDto } from './dto/createUser.dto';
import { ValidateCreatingDto } from './dto/validateCreating.dto';
import { ValidateDto } from './dto/validate.dto';
import { RTAuthGuard } from './guards/rt.guard';
import { GetUser, Public } from '../common/decorators';
import { ChangePassDto } from './dto/changepass.dto';
import { Request } from 'express';
import { getHeadersInfo } from '../common/utils/Utils';
import { AdminStrategy } from "./strategies/admin.strategy";
import { AdminAuthGuard } from "./guards/admin.guard";

@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Public()
  @Post(`CreateUser`)
  @HttpCode(HttpStatus.CREATED)
  async createUser(
    @Body() userInfo: createUserDto,
    @Req() req: Request,
    @Ip() ip,
  ) {
    return await this.authService.createUser(userInfo, {
      ip,
      ...getHeadersInfo(req.headers),
    });
  }

  @Public()
  @Post(`ValidateCreatingUser`)
  @HttpCode(HttpStatus.OK)
  async validateCreatingUser(@Body() userInfo: ValidateCreatingDto) {
    const data = await this.authService.validateCreatingUser(userInfo);
    return data;
  }

  @Public()
  @Post(`Login`)
  @HttpCode(HttpStatus.OK)
  async login(@Body() userInfo: ValidateDto, @Req() req: Request, @Ip() ip) {
    return await this.authService.login(userInfo, {
      ip,
      ...getHeadersInfo(req.headers),
    });
  }

  @Post(`logout`)
  @HttpCode(HttpStatus.OK)
  async logout(
    @GetUser('sub') userId: number,
    @GetUser('sessionId') sessionId: number,
  ) {
    return await this.authService.logout(userId, sessionId);
  }

  @Get(`sessions`)
  @HttpCode(HttpStatus.OK)
  async getSessions(@GetUser('sub') userId: number) {
    return this.authService.getSessions(userId);
  }

  @Delete(`sessions`)
  @HttpCode(HttpStatus.OK)
  async delSession(
    @GetUser('sub') userId: number,
    @Body() { id }: { id: number },
  ) {
    await this.authService.logout(userId, id);
    return this.authService.getSessions(userId);
  }

  @Post(`check`)
  @HttpCode(HttpStatus.OK)
  async check(@GetUser('sub') userId: number) {
    return await this.authService.check(userId);
  }

  @Delete(`allSessions`)
  @HttpCode(HttpStatus.OK)
  async allRemoveSession(
    @GetUser('sub') userId: number,
    @GetUser('sessionId') sessionId: number,
  ) {
    await this.authService.logoutFromEvery(userId, sessionId);
    return this.authService.getSessions(userId);
  }

  @Public()
  @UseGuards(RTAuthGuard)
  @Get(`refresh`)
  @HttpCode(HttpStatus.OK)
  async refresh(
    @GetUser('sub') userId: number,
    @GetUser('refreshToken') token: string,
    @GetUser('sessionId') sessionId: number,
  ) {
    return await this.authService.refresh(userId, token, sessionId);
  }

  @Post(`changepassword`)
  @HttpCode(HttpStatus.OK)
  async changePassword(
    @Body() userInfo: ChangePassDto,
    @GetUser('sub') userId: number,
  ) {
    const data = await this.authService.changePassword(userId, userInfo);
    return data;
  }
}
