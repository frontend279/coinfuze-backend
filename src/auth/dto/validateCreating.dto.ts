import { IsString } from 'class-validator';

export class ValidateCreatingDto {
  @IsString()
  login: string;

  @IsString()
  email: string;
}
