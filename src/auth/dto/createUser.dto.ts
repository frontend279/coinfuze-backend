import { IsString, MaxLength, MinLength } from 'class-validator';
import { Type } from 'class-transformer';

export class UserInfoDto {
  @IsString()
  phone: string;

  @IsString()
  city: string;

  @IsString()
  country: string;

  @IsString()
  addressFirst: string;

  @IsString()
  addressLast: string;

  @IsString()
  firstName: string;

  @IsString()
  lastName: string;

  @IsString()
  postalCode: string;

  @IsString()
  birthday: string;

  @IsString()
  middleName: string;
}

export class createUserDto {
  @IsString()
  @MinLength(5)
  @MaxLength(16)
  login: string;

  @IsString()
  @MinLength(6)
  @MaxLength(216)
  password: string;

  @IsString()
  email: string;

  @Type(() => UserInfoDto)
  userInfo: UserInfoDto;
}
