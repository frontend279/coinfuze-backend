import { IsString, MaxLength, MinLength } from 'class-validator';

export class ChangePassDto {
  @IsString()
  @MinLength(6)
  @MaxLength(216)
  previousPassword: string;

  @IsString()
  @MinLength(6)
  @MaxLength(216)
  currentPassword: string;
}
