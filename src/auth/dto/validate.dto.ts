import { IsString } from 'class-validator';

export class ValidateDto {
  @IsString()
  login: string;

  @IsString()
  password: string;
}
