export interface IHeadersInfo {
  ip: string;
  os: string;
  userAgent: string;
  browser: string;
}
