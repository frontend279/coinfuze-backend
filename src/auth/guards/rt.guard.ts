import { ExecutionContext, Injectable } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { Observable } from 'rxjs';
import { PrismaService } from '../../prisma.service';

@Injectable()
export class RTAuthGuard extends AuthGuard('r-token') {
  constructor(private prisma: PrismaService) {
    super();
  }

  canActivate(
    context: ExecutionContext,
  ): boolean | Promise<boolean> | Observable<boolean> {
    return this.validateRequest(context);
  }

  async validateRequest(execContext: ExecutionContext): Promise<boolean> {
    const request = execContext.switchToHttp().getRequest();
    if (await super.canActivate(execContext)) {
      return true;
    } else {
      // this.prisma.sessions.deleteMany({
      //   where: {
      //     refreshToken: request.headers.authorazation,
      //   },
      // });
    }
  }
}
