import {
  ForbiddenException,
  HttpException,
  HttpStatus,
  Injectable,
} from '@nestjs/common';
import { createUserDto } from './dto/createUser.dto';
import { PrismaService } from '../prisma.service';
import * as crypto from 'bcrypt';
import { JwtService } from '@nestjs/jwt';
import { ValidateDto } from './dto/validate.dto';
import { ValidateCreatingDto } from './dto/validateCreating.dto';
import * as process from 'process';
import { ChangePassDto } from './dto/changepass.dto';
import { UsersService } from '../users/users.service';
import { formatTimeString } from '../common/utils/Time';
import { IHeadersInfo } from './interfaces/IHeadersInfo';

@Injectable()
export class AuthService {
  constructor(
    private prisma: PrismaService,
    private jwtService: JwtService,
    private users: UsersService,
  ) {}

  async validateCreatingUser(userInfo: ValidateCreatingDto) {
    return this.users.IsUserNotExists(userInfo.login, userInfo.email);
  }
  async createUser(userInfo: createUserDto, headersInfo: IHeadersInfo) {
    if (this.users.IsUserNotExists(userInfo.login, userInfo.email)) {
      const encryptedPassword = await this.hashData(userInfo.password);
      const user = await this.prisma.users.create({
        data: {
          password: encryptedPassword,
          login: userInfo.login,
          email: userInfo.email,
          userInfo: {
            create: {
              ...userInfo.userInfo,
            },
          },
        },
        include: {
          userInfo: true,
        },
      });

      const session = await this.createSession(user.id);
      const tokens = await this.getTokens(
        user.id,
        user.login,
        user.role,
        session.id,
      );
      await this.updateRtHash(
        tokens.refresh_token,
        session.id,
        headersInfo.browser,
        headersInfo.userAgent,
        headersInfo.ip,
        headersInfo.os,
      );
      return {
        login: user.login,
        email: user.email,
        balance: user.balance,
        createdAt: formatTimeString(user.createdAt).date,
        balanceCurrency: user.balanceCurrency,
        ...tokens,
        ...user.userInfo,
      };
    } else {
      throw new HttpException(
        'Пользователь уже существует',
        HttpStatus.BAD_REQUEST,
      );
    }
  }
  async login(user: ValidateDto, headersInfo: IHeadersInfo) {
    const userDB = await this.prisma.users.findFirst({
      where: {
        OR: [{ login: user.login }, { email: user.login }],
      },
      include: {
        userInfo: true,
      },
    });
    if (!userDB || !(await crypto.compare(user.password, userDB.password))) {
      throw new HttpException(
        'Неверный логин или пароль',
        HttpStatus.FORBIDDEN,
      );
    } else {
      const session = await this.createSession(userDB.id);
      const tokens = await this.getTokens(
        userDB.id,
        userDB.login,
        userDB.role,
        session.id,
      );
      await this.updateRtHash(
        tokens.refresh_token,
        session.id,
        headersInfo.browser,
        headersInfo.userAgent,
        headersInfo.ip,
        headersInfo.os,
      );

      return {
        login: userDB.login,
        createdAt: formatTimeString(userDB.createdAt).date,
        email: userDB.email,
        balance: userDB.balance,
        balanceCurrency: userDB.balanceCurrency,
        ...tokens,
        ...userDB.userInfo,
      };
    }
  }
  async refresh(userID: number, token: string, sessionId: number) {
    const session = await this.prisma.sessions.findUnique({
      where: {
        id: sessionId,
      },
      include: {
        user: true,
      },
    });
    if (!session || session.userId !== userID) throw new ForbiddenException();
    const rtMatches = await crypto.compare(token, session.refreshToken);
    if (!rtMatches) throw new ForbiddenException();

    const tokens = await this.getTokens(
      session.user.id,
      session.user.login,
      session.user.role,
      session.id,
    );
    return tokens;
  }

  async logout(userId: number, sessionId: number) {
    await this.prisma.sessions.deleteMany({
      where: {
        userId: userId,
        id: sessionId,
      },
    });
  }

  async logoutFromEvery(userId: number, sessionId: number) {
    await this.prisma.sessions.deleteMany({
      where: {
        userId: userId,
        id: {
          not: sessionId,
        },
      },
    });
  }

  async getSessions(userId: number) {
    return this.prisma.sessions.findMany({
      where: {
        userId: userId,
      },
    });
  }

  async check(userId: number) {
    const userDB = await this.prisma.users.findFirst({
      where: {
        id: userId,
      },
      include: {
        userInfo: true,
      },
    });
    return {
      login: userDB.login,
      createdAt: formatTimeString(userDB.createdAt).date,
      email: userDB.email,
      balance: userDB.balance,
      balanceCurrency: userDB.balanceCurrency,
      ...userDB.userInfo,
    };
  }

  async changePassword(userId: number, data: ChangePassDto) {
    const userDB = await this.prisma.users.findFirst({
      where: {
        id: userId,
      },
    });
    if (
      !userDB ||
      !(await crypto.compare(data.previousPassword, userDB.password))
    ) {
      throw new HttpException('Forbidden', HttpStatus.FORBIDDEN);
    } else {
      console.log(1);
      const encryptedPass = await this.hashData(data.currentPassword);
      await this.prisma.users.update({
        where: {
          id: userId,
        },
        data: {
          password: encryptedPass,
        },
      });
      return 200;
    }
  }

  async getTokens(
    userId: number,
    username: string,
    role: string,
    sessionId: number,
  ) {
    const payload = {
      username: username,
      sub: userId,
      role: role,
      sessionId: sessionId,
    };

    const [at, rt] = await Promise.all([
      this.jwtService.signAsync(payload, {
        expiresIn: 60 * 1,
        secret: process.env.AUTH_SECRET_AT,
      }),
      this.jwtService.signAsync(payload, {
        expiresIn: 60 * 60 * 24 * 7,
        secret: process.env.AUTH_SECRET_RT,
      }),
    ]);
    return {
      access_token: at,
      refresh_token: rt,
    };
  }

  async hashData(value: string) {
    return await crypto.hash(value, 10);
  }

  async createSession(userId: number) {
    return this.prisma.sessions.create({
      data: {
        userId: userId,
      },
    });
  }

  async updateRtHash(
    rt: string,
    sessionId: number,
    browser: string,
    userAgent: string,
    ip: string,
    os: string,
  ) {
    const hash = await this.hashData(rt);
    await this.prisma.sessions.update({
      where: {
        id: sessionId,
      },
      data: {
        refreshToken: hash,
        browser,
        userAgent,
        ip,
        os,
      },
    });
  }
}
