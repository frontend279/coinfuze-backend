import { IsNumber } from 'class-validator';

export class InvestPlanDto {
  @IsNumber()
  id: number;

  @IsNumber()
  summary: number;
}
