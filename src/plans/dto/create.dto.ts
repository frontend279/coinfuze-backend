import {
  IsBoolean,
  IsNumber,
  IsOptional,
  IsString,
} from 'class-validator';
import { Statuses } from '@prisma/client';
export class CreatePlanDto {
  @IsBoolean()
  @IsOptional()
  isActive?: boolean;

  @IsNumber()
  minInvest: number;

  @IsString()
  time?: string;

  @IsNumber()
  waitingFinanceResult: number;

  @IsString()
  startDate: string;

  @IsString()
  endDate: string;

  @IsNumber()
  @IsOptional()
  startFund?: number;

  @IsNumber()
  @IsOptional()
  addedFund?: number;

  @IsNumber()
  @IsOptional()
  financeResult?: number;

  @IsNumber()
  @IsOptional()
  profit?: number;

  @IsNumber()
  @IsOptional()
  status?: Statuses;

  @IsNumber()
  @IsOptional()
  investedMoney?: number;

  @IsNumber()
  @IsOptional()
  planCurrency?: string;
}
