import {
  Controller,
  Get,
  Body,
  Param,
  Delete,
  HttpStatus,
  HttpCode,
  Post,
} from '@nestjs/common';
import { PlansService } from './plans.service';
import { GetUser } from '../common/decorators';
import { InvestPlanDto } from "./dto/InvestPlan.dto";

@Controller('plans')
export class PlansController {
  constructor(private readonly plansService: PlansService) {}

  @Get('/available')
  @HttpCode(HttpStatus.OK)
  findAllAvailable(@Body() { planId }: { planId: number }) {
    return this.plansService.findAllAvailable(planId);
  }

  @Get('/findUserPlans')
  @HttpCode(HttpStatus.OK)
  findUserPlans(@GetUser('sub') sub: string) {
    return this.plansService.findUserPlans(+sub);
  }

  @Post('/investOne')
  @HttpCode(HttpStatus.OK)
  investOne(@GetUser('sub') sub: string, @Body() investing: InvestPlanDto) {
    return this.plansService.investOne(+sub, investing);
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.plansService.findOne(+id);
  }

  @Post('/pick')
  pickOne(@GetUser('sub') sub: string, @Body() { id }: { id: number }) {
    console.log(sub);
    console.log(id);
    return this.plansService.pickOne(+sub, id);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.plansService.remove(+id);
  }
}
