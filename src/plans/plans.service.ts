import { Injectable, NotFoundException } from '@nestjs/common';
import { CreatePlanDto } from './dto/create.dto';
import { PrismaService } from '../prisma.service';
import { UpdatePlanDto } from './dto/update.dto';
import { InvestPlanDto } from './dto/InvestPlan.dto';

@Injectable()
export class PlansService {
  constructor(private prisma: PrismaService) {}

  async create(createPlanDto: CreatePlanDto) {
    const plan = await this.prisma.investPlans.create({
      data: createPlanDto,
    });
    return {
      id: plan.id,
    };
  }

  async pickOne(sub: number, planId: number) {
    await this.prisma.investings.create({
      data: {
        userId: sub,
        investPlansId: planId,
      },
    });
    const plan = await this.prisma.investPlans.findUnique({
      where: {
        id: planId,
      },
    });
    return plan;
  }

  async investOne(sub: number, data: InvestPlanDto) {
    await this.prisma.investings.updateMany({
      where: {
        investPlansId: data.id,
      },
      data: {
        isActivated: true,
        money: data.summary,
      },
    });
    const plan = await this.prisma.investPlans.findUnique({
      where: {
        id: data.id,
      },
    });
    await this.prisma.investPlans.update({
      where: {
        id: data.id,
      },
      data: {
        investedMoney: plan.investedMoney + data.summary,
      },
    });
    return 200;
  }

  async findUserPlans(sub: number) {
    const plans = await this.prisma.investings.findMany({
      where: {
        userId: sub,
      },
      include: {
        plan: true,
      },
    });
    return plans;
  }

  async findAllAvailable(sub: number) {
    const plans = await this.prisma.investPlans.findMany({
      where: {
        isActive: false,
        Investings: {
          none: {
            userId: {
              not: sub,
            },
          },
        },
      },
      select: {
        id: true,
        startDate: true,
        endDate: true,
        minInvest: true,
        waitingFinanceResult: true,
        planCurrency: true,
      },
    });
    return plans;
  }

  async findAll() {
    const plans = await this.prisma.investPlans.findMany({
      select: {
        id: true,
      },
    });
    return plans;
  }

  async findOne(id: number) {
    const plan = await this.prisma.investPlans.findFirst({
      where: {
        id,
      },
      include: {
        Investings: {
          include: {
            user: true,
          },
        },
      },
    });
    if (plan) {
      return plan;
    } else {
      throw new NotFoundException();
    }
  }

  async remove(id: number) {
    const plan = await this.prisma.investPlans.delete({
      where: {
        id,
      },
    });
    if (plan) {
      return plan;
    } else {
      throw new NotFoundException();
    }
  }

  async changeOne(id: number, data: UpdatePlanDto) {
    const plan = await this.prisma.investPlans.update({
      where: {
        id,
      },
      data: {
        ...data,
      },
    });
    return plan;
  }
}
