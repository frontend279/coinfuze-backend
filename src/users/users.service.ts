import { Injectable, NotFoundException } from '@nestjs/common';
import { PrismaService } from '../prisma.service';
import { idDto } from './dto/id.dto';
import { paginationDto } from './dto/pagination.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import * as fs from 'fs';

@Injectable()
export class UsersService {
  constructor(private prisma: PrismaService) {}

  async IsUserNotExists(login: string, email: string) {
    const userDB = await this.prisma.users.findFirst({
      where: {
        OR: [{ login: login }, { email: email }],
      },
    });
    if (userDB) {
      return {
        validate: false,
      };
    } else {
      return {
        validate: true,
      };
    }
  }

  async fetchOneByID({ id }: idDto) {
    try {
      const userDB = await this.prisma.users.findFirst({
        where: { id: +id },
      });
      if (userDB) {
        return userDB;
      } else {
        throw new NotFoundException('Пользователь не найден');
      }
    } catch (e) {
      throw e;
    }
  }

  async fetchOneByLogin(login: string) {
    try {
      const userDB = await this.prisma.users.findFirst({
        where: { login },
      });
      if (userDB) {
        return userDB;
      } else {
        throw new NotFoundException('Пользователь не найден');
      }
    } catch (e) {
      throw e;
    }
  }

  async fetchAll({ take, skip }: paginationDto) {
    try {
      const users = await this.prisma.users.findMany({
        skip,
        take,
      });
      return users;
    } catch (e) {
      throw e;
    }
  }

  async updateAvatar(filename: string, id: number) {
    if (filename) {
      const userDB = await this.prisma.users.findFirst({
        where: {
          id,
        },
        include: {
          userInfo: true,
        },
      });
      if (userDB.userInfo.avatar !== '') {
        fs.unlinkSync('./uploads/avatars/' + userDB.userInfo.avatar);
      }

      const user = await this.prisma.users.update({
        where: {
          id,
        },
        include: {
          userInfo: true,
        },
        data: {
          userInfo: {
            update: {
              avatar: filename,
            },
          },
        },
      });
      if (user) {
        return user;
      } else {
        throw new NotFoundException();
      }
    }
  }

  async updateUserInfo(id: number, data: UpdateUserDto, filename: string) {
    if (filename) await this.updateAvatar(filename, id);
    const user = await this.prisma.users.update({
      where: {
        id,
      },
      data: {
        email: data.email,
        userInfo: {
          update: {
            data: {
              ...data.userInfo,
            },
          },
        },
      },
      include: {
        userInfo: true,
      },
    });
    if (user) {
      return {
        email: user.email,
        ...user.userInfo,
      };
    } else {
      throw new NotFoundException();
    }
  }
}
