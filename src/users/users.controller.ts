import {
  Body,
  Controller,
  Get,
  Param,
  Patch,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import { UsersService } from './users.service';
import { idDto } from './dto/id.dto';
import { paginationDto } from './dto/pagination.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import { FileInterceptor } from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import e from 'express';
import { v4 as uuidv4 } from 'uuid';
import * as path from 'path';
import { GetUser, Public } from '../common/decorators';

@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Get('fetchbyid/:id')
  @Public()
  async fetchOneByID(@Param() params: idDto) {
    return await this.usersService.fetchOneByID(params);
  }

  @Get('fetchAll')
  async fetchAll(@Body() params: paginationDto) {
    return await this.usersService.fetchAll(params);
  }

  @Patch('updateUserInfo')
  @UseInterceptors(
    FileInterceptor('file', {
      storage: diskStorage({
        destination: './uploads/avatars',
        filename(
          req: e.Request,
          file: Express.Multer.File,
          callback: (error: Error | null, filename: string) => void,
        ) {
          const filename = uuidv4();
          const extension = path.parse(file.originalname).ext;
          callback(null, `${filename}${extension}`);
        },
      }),
    }),
  )
  async updateUserInfo(
    @UploadedFile() file,
    @GetUser('sub') id: string,
    @Body() updateUserDto: UpdateUserDto,
  ) {
    return this.usersService.updateUserInfo(+id, updateUserDto, file?.filename);
  }
}
