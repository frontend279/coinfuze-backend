import { IsNumber } from 'class-validator';

export class paginationDto {
  @IsNumber()
  skip?: number;

  @IsNumber()
  take?: number;
}
