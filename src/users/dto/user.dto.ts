import { IsString, MaxLength, MinLength } from 'class-validator';
import { Type } from 'class-transformer';

export class UserInfoDto {
  @IsString()
  phone: string;

  @IsString()
  city: string;

  @IsString()
  country: string;

  @IsString()
  addressFirst: string;

  @IsString()
  addressLast: string;

  @IsString()
  firstName: string;

  @IsString()
  lastName: string;

  @IsString()
  postalCode: string;

  @IsString()
  birthday: string;

  @IsString()
  middleName: string;

  @IsString()
  lang: string;
}

export class UserDto {
  @IsString()
  email: string;

  @Type(() => UserInfoDto)
  userInfo: UserInfoDto;
}
